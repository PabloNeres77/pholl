const card = document.querySelector('.image-carrousel')
const titles = [
    'Traço elétrico auto regulável',
    'Elemento resistivo tubular',
    'Tubular flexível',
    'Capa térmica',
    'Marmiteiro',
    'Radiador elétrico para aquecimento de ar',
    'Resistência elétrica tubular para imersão',
    'Aquecedor elétrico de passagem',
    'Resistência elétrica em cartucho',
    'Resistência elétrica fundida',
    'Mangueiras térmicas'
]

let counter = 0
let index = 1

function next() {
    console.log(index)
    const item = document.querySelector('.card-carrousel')
    item.style.transform = 'translateX(-1000px)'
    
    setTimeout(() => {
        item.style.transform = 'translateX(0)'
    }, 300);

    if (index === 11) {
        document.querySelector('.arrow-right').style.visibility =  'hidden'
        return
    }
    index++
    card.innerHTML = `<img src="./assets/Imagens/portfolio/${index}.jpg">`
    if (index >= 2) {
        document.querySelector('.arrow-left').style.visibility =  'visible'
    }
}

function back() {
    console.log(index)
    if (index === 1) {
        document.querySelector('.arrow-left').style.visibility =  'hidden'
        document.querySelector('.arrow-right').style.visibility =  'visible'
        return
    }
    index--
    card.innerHTML = `<img src="./assets/Imagens/portfolio/${index}.jpg">`
}